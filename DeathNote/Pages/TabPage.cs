﻿using System;

using Xamarin.Forms;

namespace DeathNote
{
	public class TabPage : TabbedPage
	{
		public TabPage ()
		{
			this.Children.Add (new TanıtımPage());
			this.Children.Add (new KarakterlerPage());
			this.Children.Add (new DefterKurallarıPage());
			this.Children.Add (new SozlukPage());
            this.Children.Add(new IzlePage());
		}
	}
}


