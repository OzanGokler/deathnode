﻿using System;
using Xamarin.Forms;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;


namespace DeathNote
{
	public class KarakterlerPage : ContentPage
	{
        Karakterler _karakterler = new Karakterler();
        ListView lvKarakter;
        public KarakterlerPage()
        {
            this.Title = "Karakterler";

            lvKarakter = new ListView
            {
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            

			//get Json and read File
			Assembly assembly = typeof(App).GetTypeInfo().Assembly;
			Stream stream = assembly.GetManifestResourceStream("DeathNote.Assets.Karakterler.json");
			string text = "";
           	using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

			_karakterler = JsonConvert.DeserializeObject<Karakterler>(text);

            lvKarakter.ItemTemplate = new DataTemplate(typeof(KarakterListCell));
            lvKarakter.HasUnevenRows = true;
            lvKarakter.ItemsSource = null;

            //for(int i=0; i<_karakterler._Karakterler.Count; i++ )
            //{
            //    lvKarakter.ItemsSource = _karakterler._Karakterler[i].sGercekIsim;
            //    lvKarakter.ItemsSource = _karakterler._Karakterler[i].sTakmaIsim;
            //    lvKarakter.ItemsSource = _karakterler._Karakterler[i].sDogumTarihi;
            //    lvKarakter.ItemsSource = _karakterler._Karakterler[i].sOlumTarihi;

            //}


            lvKarakter.ItemsSource = _karakterler._Karakterler.Select(x =>
            {
                return new KarakterListCellHolder
                {
                    //BURDA SIKINTI VAR TEKRARLAMA YAPIYOR (isimler 2 3 kez aynı yazılıyor) ve IMAGE CALISMIYOR
                    avatar =  new Image { Source = ImageSource.FromFile(x.iResimYolu) },
                    sGercekIsim = x.sGercekIsim,
                    sTakmaIsim = x.sTakmaIsim,
                    sDogumTarihi = x.sDogumTarihi,
                    sOlumTarihi = x.sOlumTarihi,
                    sBoy = x.sBoy,
                    sKilo = x.sKilo,
                    sKanGurubu = x.sKanGurubu,
                    sSevdikleri = x.sSevdikleri,
                    sSevmedikleri = x.sSevmedikleri,
                    sMeslek = x.sMeslek,
                    sDeathNote = x.sDeathNote,
                    sSeslendiren = x.sSeslendiren,
                    sHakkinda = x.sHakkinda
                };
            });

            

            var stack = new StackLayout { VerticalOptions = LayoutOptions.FillAndExpand };

            Content = new StackLayout
            {
                Children =
                {
                    lvKarakter
                }
            };
		}      
        

	}
}