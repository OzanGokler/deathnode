﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using System.Linq;

namespace DeathNote
{
	public class DefterKurallarıPage : ContentPage
	{
		public DefterKurallarıPage ()
		{
			this.Title = "Defter Kuralları";

            Kurallar _kurallar = new Kurallar();
            ListView lvKural;

            lvKural = new ListView
            {
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Assembly assembly = typeof(App).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("DeathNote.Assets.Kurallar.json");
            string text = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

            _kurallar = JsonConvert.DeserializeObject<Kurallar>(text);

            lvKural.ItemTemplate = new DataTemplate(typeof(KuralListCell));
            lvKural.HasUnevenRows = true;
            lvKural.ItemsSource = null;


            lvKural.ItemsSource = _kurallar._Kurallar.Select(x =>
            {
                return new KurallarListCellHolder
                {
                    Kural = x.kural
                };
            });

            Content = new StackLayout
            {
                Children =
                {
                    lvKural,

                }
            };
		}
	}
}


