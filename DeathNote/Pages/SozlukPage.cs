﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using System.Linq;

namespace DeathNote
{
	public class SozlukPage : ContentPage
	{
		public SozlukPage ()
		{
            Sozcukler _sozcukler = new Sozcukler();
            ListView lvSozcukler;

            this.Title = "Sözcükler";

            lvSozcukler = new ListView
            {
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            Assembly assembly = typeof(App).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("DeathNote.Assets.Sozluk.json");
            string text = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

            _sozcukler = JsonConvert.DeserializeObject<Sozcukler>(text);

            lvSozcukler.ItemTemplate = new DataTemplate(typeof(SozcukListCell));
            lvSozcukler.HasUnevenRows = true;
            lvSozcukler.ItemsSource = null;


            lvSozcukler.ItemsSource = _sozcukler._Sozcukler.Select(x =>
            {
                return new SozlukListCellHolder 
                {
                    sozcuk = x.sozcuk
                };
            });

            Content = new StackLayout
            {
                Children =
                {
                    lvSozcukler
                }
            };
		}
	}
}


