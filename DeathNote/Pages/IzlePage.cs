﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using System.Linq;


namespace DeathNote
{
    public class IzlePage : ContentPage
    {
        public IzlePage()
        {
            this.Title = "İzle";
            Image iAfis = new Image
            {
                Aspect = Aspect.AspectFit
            };
            iAfis.Source = ImageSource.FromResource("DeathNote.Content.Avatarlar.Afis.png");

            Button btnIzle = new Button
            {
                Text = "İZLE",
                WidthRequest = 150,
                HeightRequest = 50,
                BackgroundColor = Color.Aqua,
                TextColor = Color.Red
            };

            btnIzle.Clicked += btnIzle_Clicked;

            var layout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = 0,
                Children = {
                    iAfis,
                    btnIzle
                }
            };

            var scrolView = new ScrollView{Content = layout};
            Content = scrolView;
        }

        void btnIzle_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.youtube.com/watch?v=8-XIOtn8_W4&index=1&list=PLTY4Qb_BMSxoLREakyxOOiAFx1f-9wW85"));
        }

        
    }
}
