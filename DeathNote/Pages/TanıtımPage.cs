﻿using System;

using Xamarin.Forms;

namespace DeathNote
{
	public class TanıtımPage : ContentPage
	{
		public TanıtımPage ()
		{
            this.Title = "Tanıtım";
            //this.BackgroundImage = "DeathNote.Content.Avatarlar.L.png";

            Image iDeathNote = new Image
            {
                Aspect = Aspect.AspectFit 
            };
            iDeathNote.Source = ImageSource.FromResource("DeathNote.Content.Avatarlar.afis.png");

            Label iTanitim = new Label
            {
                TextColor = Color.Aqua,
            };
            iTanitim.Text = "Hikayemiz, Ryuk adında bir ölüm meleğinin (Shinigami), Death Note (Ölüm Defteri)'ni dünyada kaybetmesiyle başlar."
            + " Ölüm Defteri, bir okulun bahçesine düşer. Bu okulun öğrencilerinden Light defteri bulur, üzerinde nasıl kullanıldığı yazmaktadır." 
            + " Light, buna inanmamaktadır. İlk denemesiyle birlikte bunun gerçekliğine inanmaya başlar."
            + " Defterin kurallarına göre 5 gün boyunca kötü olduğunu düşündüğü yüzlerce kişi öldürdükten sonra, Ryuk adında bir Shinigami, Light'ın odasına girer."
            + " Onun kendisini öldüreceğini düşünen Light korkmaya başlar."
            + " Buna karşın Ryuk Light'a birşey yapmayacağını söyler, çünkü defteri eline alan kişi onun sahibi olur." 
            + " Kötü yanı ise bu defteri kullanan cennet ya da cehennemden hiçbirine giremez."
            + " Light, Ölüm defteri'ni kullanarak kendi adaletini dünya üzerinde uygularken, dünyanın en iyi dedektiflerinden 'L' onu bulmaya çalışacaktır." 
            + " Herkesin Gizli Güç Kira diye tanıdığı Light, hem kimliğini gizlemeli hemde bu dedektifi öncelikle bulmalıdır." 
            + " Çünkü eğer yakalanırsa idam edilecektir, Kısaca kim kimi ilk bulursa ölecek...";

            var layout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = 0,
                Children = {
                    iDeathNote,
                    iTanitim
                }
            };

            var scrollView = new ScrollView { Content = layout };
            Content = scrollView;
		}
	}
}


