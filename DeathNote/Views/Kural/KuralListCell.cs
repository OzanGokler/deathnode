﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DeathNote
{
    class KuralListCell : ViewCell
    {
        public KuralListCell()
        {
            Label lblKural = new Label
            {
                TextColor = Color.White,
                FontSize = 14,
                XAlign = TextAlignment.Start,
            };

            lblKural.SetBinding(Label.TextProperty, "Kural");

            var stckRoot = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Spacing = 7,
                Padding = 7,
                Children = {
					lblKural,
				}
            };

            this.View = stckRoot;
        }
    }
}
