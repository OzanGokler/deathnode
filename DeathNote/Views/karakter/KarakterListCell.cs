﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace DeathNote
{
    public class KarakterListCell : ViewCell
    {


        public KarakterListCell()
        {
            Image imgPoster = new Image
            {
                WidthRequest = 100,
                HeightRequest = 125,
                Aspect = Aspect.AspectFit
            };

            imgPoster.SetBinding(Image.SourceProperty, "avatar");
            
            Label lblGercekIsim = new Label
            {
                TextColor = Color.White,
                FontSize = 14,
                XAlign = TextAlignment.Start,
            };

            lblGercekIsim.SetBinding(Label.TextProperty, "sGercekIsim");

            var lblTakmaIsim = new Label
            {
                TextColor = Color.White,
                FontSize = 14,
                XAlign = TextAlignment.Start,
            };

            lblTakmaIsim.SetBinding(Label.TextProperty, "sTakmaIsim");

            var lblDogumTarihi = new Label
            {
                TextColor = Color.White,
                FontSize = 14,
                XAlign = TextAlignment.Start,
            };

            lblDogumTarihi.SetBinding(Label.TextProperty, "sDogumTarihi");

            var lblOlumTarihi = new Label
            {
                TextColor = Color.White,
                FontSize = 14,
                XAlign = TextAlignment.Start,
            };

            lblOlumTarihi.SetBinding(Label.TextProperty, "sOlumTarihi");

		
			var lblBoy = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblBoy.SetBinding(Label.TextProperty, "sBoy");


			var lblKilo = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblKilo.SetBinding(Label.TextProperty, "sKilo");


			var lblKanGurubu = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblKanGurubu.SetBinding(Label.TextProperty, "sKanGurubu");

			var lblSevdikleri = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblSevdikleri.SetBinding(Label.TextProperty, "sSevdikleri");

			var lblSevmedikleri = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblSevmedikleri.SetBinding(Label.TextProperty, "sSevmedikleri");

			var lblMeslek = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblMeslek.SetBinding(Label.TextProperty, "sMeslek");

			var lblDeathNote = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblDeathNote.SetBinding(Label.TextProperty, "sDeathNote");


			var lblSeslendiren = new Label
			{
				TextColor = Color.White,
				FontSize = 14,
				XAlign = TextAlignment.Start,
			};

			lblSeslendiren.SetBinding(Label.TextProperty, "sSeslendiren");

            var lblKakkinda = new Label
            {
                TextColor = Color.Red,
                FontSize = 14,
                XAlign = TextAlignment.Start,
                BackgroundColor = Color.Aqua,
            };

            lblKakkinda.SetBinding(Label.TextProperty, "sHakkinda");

            StackLayout stckTanitim = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 2,
                Children = {
                    lblKakkinda,					
                }
            };

            var stckInfo = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Spacing = 10,
                Padding = 6,
                Children = {
					lblGercekIsim,
                    lblTakmaIsim,
                    lblDogumTarihi,
                    lblOlumTarihi,
					lblBoy,
					lblKilo,
					lblKanGurubu,
					lblSevdikleri,
					lblSevmedikleri,
					lblMeslek,
					lblDeathNote,
					lblSeslendiren,
					stckTanitim
				}
            };
            
            var stckRoot = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Spacing = 7,
                Padding = 7,
                Children = {
					imgPoster,
					stckInfo
				}
            };

            this.View = stckRoot;
            
        }
    }
}
