﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DeathNote
{
    class SozcukListCell : ViewCell
    {
        public SozcukListCell()
        {
            Label lblSozcuk = new Label
            {
                TextColor = Color.White,
                FontSize = 14,
                XAlign = TextAlignment.Start,
            };

            lblSozcuk.SetBinding(Label.TextProperty, "sozcuk");


            Label lblAnlam = new Label
            {
                TextColor = Color.White,
                FontSize = 14,
                XAlign = TextAlignment.Start,
            };

            lblAnlam.SetBinding(Label.TextProperty, "anlam");

            var stckRoot = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Spacing = 7,
                Padding = 7,
                Children = {
					lblSozcuk,
				}
            };

            this.View = stckRoot;
        }
    }
}
