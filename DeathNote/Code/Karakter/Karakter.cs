﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace DeathNote
{
    public class Karakter : ContentPage
    {
		public string iResimYolu { get; set; }
        public string sGercekIsim { get; set; }
        public string sTakmaIsim { get; set; }
        public string sDogumTarihi { get; set; }
        public string sOlumTarihi { get; set; }
        public string sBoy { get; set; }
        public string sKilo { get; set; }
        public string sKanGurubu { get; set; }
        public string sSevdikleri { get; set; }
        public string sSevmedikleri { get; set; }
        public string sMeslek { get; set; }
        public string sDeathNote { get; set; }
        public string sSeslendiren { get; set; }
        public string sHakkinda { get; set; }
    }
}
